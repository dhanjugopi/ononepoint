<?php

add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles', 100000 );

function salient_child_enqueue_styles() {

	$nectar_theme_version = nectar_get_theme_version();
	wp_enqueue_style( 'salient-child-style', get_stylesheet_directory_uri() . '/style.css', array( 'dynamic-css' ), $nectar_theme_version );

	if ( is_rtl() ) {
		wp_enqueue_style( 'salient-rtl', get_template_directory_uri() . '/rtl.css', array(), '1', 'screen' );
	}


	//enqueue font
	wp_enqueue_style( 'be-vietnam-font', 'https://fonts.googleapis.com/css2?family=Be+Vietnam:wght@300;400;700&display=swap', false );
}


function register_widget_areas() {

	register_sidebar( array(
		'name'          => 'Footer area top',
		'id'            => 'footer_area_top',
		'description'   => 'TOp Footer',
		'before_widget' => '<section class="footer-area footer-area-top"><div class="container-fluid">',
		'after_widget'  => '</div></section>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

}

add_action( 'widgets_init', 'register_widget_areas' );